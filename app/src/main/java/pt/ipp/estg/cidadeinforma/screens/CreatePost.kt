package pt.ipp.estg.cidadeinforma.screens

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import pt.ipp.estg.cidadeinforma.database.Post
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import pt.ipp.estg.cidadeinforma.database.TypePost
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.Priority
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import pt.ipp.estg.cidadeinforma.API.MyGeoApi
import pt.ipp.estg.cidadeinforma.API.RetrofitHelper
import pt.ipp.estg.cidadeinforma.viewModels.PostsViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.Date
import java.util.UUID


/**
 * Tela Composable para interação com o Firestore.
 *
 * @param navController O controlador de navegação.
 */
@Composable
fun FirestoreScreen(navController: NavController) {
    val postTitle = rememberSaveable { mutableStateOf("") }
    val postDescription = rememberSaveable { mutableStateOf("") }
    val postPhoto = rememberSaveable { mutableStateOf("") }
    val postPostcode = rememberSaveable { mutableStateOf("") }
    val postLatitude = rememberSaveable { mutableStateOf(0.0) }
    val postLongitude = rememberSaveable { mutableStateOf(0.0) }
    val postLifespan = rememberSaveable { mutableStateOf(0) }
    val postType = rememberSaveable { mutableStateOf(TypePost.DEFAULT) }
    val viewModel: PostsViewModel = viewModel()
    val context = LocalContext.current
    val user = Firebase.auth.currentUser


    fun isWifiConnected(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val network = connectivityManager.activeNetwork ?: return false
        val networkCapabilities =
            connectivityManager.getNetworkCapabilities(network) ?: return false

        return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
    }

    val isConnected = isWifiConnected(context)

    Column {
        CreatePost(postTitle = postTitle.value,
            onPostTitleChange = { postTitle.value = it },
            postDescription = postDescription.value,
            onPostDescriptionChange = { postDescription.value = it },
            postPhoto = postPhoto.value,
            onPostPhotoChange = { postPhoto.value = it },
            postPostcode = postPostcode.value,
            onPostPostcodeChange = { postPostcode.value = it },
            postLatitude = postLatitude.value,
            onPostLatitudeChange = { postLatitude.value = it },
            postLongitude = postLongitude.value,
            onPostLongitudeChange = { postLongitude.value = it },
            postLifespan = postLifespan.value,
            onPostLifespanChange = { postLifespan.value = it },
            postType = postType.value,
            onPostTypeChange = { postType.value = it },
            navController,
            user,
            viewModel,
            onAddClick = {
                if (user != null) {
                    val postId = UUID.randomUUID().toString()
                    val currentDate = Date()
                    val timestamp = currentDate.time
                    viewModel.savePost(
                        Post(
                            postId,
                            postTitle.value,
                            postDescription.value,
                            user.email,
                            postPhoto.value,
                            postPostcode.value,
                            postLatitude.value,
                            postLongitude.value,
                            postLifespan.value,
                            0,
                            listOf(),
                            0,
                            listOf(),
                            postType.value,
                            timestamp,
                            true,
                            0.0.toFloat(),
                            listOf(),
                            listOf(),
                            listOf(),
                        )
                    )
                    viewModel.insertPost(
                        Post(
                            postId,
                            postTitle.value,
                            postDescription.value,
                            user.email,
                            postPhoto.value,
                            postPostcode.value,
                            postLatitude.value,
                            postLongitude.value,
                            postLifespan.value,
                            0,
                            listOf(),
                            0,
                            listOf(),
                            postType.value,
                            timestamp,
                            true,
                            0.0.toFloat(),
                            listOf(),
                            listOf(),
                            listOf()
                        )
                    )
                }
            }
        )
    }
}


/**
 * Função composable para criar um novo post.
 *
 * @param postTitle O título do post.
 * @param onPostTitleChange Callback para atualizar o título do post.
 * @param postDescription A descrição do post.
 * @param onPostDescriptionChange Callback para atualizar a descrição do post.
 * @param postPhoto O URI da foto do post.
 * @param onPostPhotoChange Callback para atualizar o URI da foto do post.
 * @param postPostcode O código postal da localização do post.
 * @param onPostPostcodeChange Callback para atualizar o código postal do post.
 * @param postLatitude A latitude da localização do post.
 * @param onPostLatitudeChange Callback para atualizar a latitude do post.
 * @param postLongitude A longitude da localização do post.
 * @param onPostLongitudeChange Callback para atualizar a longitude do post.
 * @param postLifespan A duração de vida do post.
 * @param onPostLifespanChange Callback para atualizar a duração de vida do post.
 * @param postType O tipo do post.
 * @param onPostTypeChange Callback para atualizar o tipo do post.
 * @param navController O controlador de navegação.
 * @param user O utilizador da Firebase logado.
 * @param viewModel O view model para os posts.
 * @param onAddClick Callback quando o utilizador clica para adicionar um novo post.
 */

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreatePost(
    postTitle: String, onPostTitleChange: (String) -> Unit,
    postDescription: String, onPostDescriptionChange: (String) -> Unit,
    postPhoto: String, onPostPhotoChange: (String) -> Unit,
    postPostcode: String, onPostPostcodeChange: (String) -> Unit,
    postLatitude: Double, onPostLatitudeChange: (Double) -> Unit,
    postLongitude: Double, onPostLongitudeChange: (Double) -> Unit,
    postLifespan: Int, onPostLifespanChange: (Int) -> Unit,
    postType: TypePost, onPostTypeChange: (TypePost) -> Unit,
    navController: NavController,
    user: FirebaseUser?,
    viewModel: PostsViewModel,
    onAddClick: (Post) -> Unit
) {
    val context = LocalContext.current

    /**
     * Função para obter a informação sobre a ligação ao Wi-Fi do dispositivo.
     * True caso esteja ligado, False caso contrário.
     *
     * @param context O contexto atual.
     */
    fun isWifiConnected(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val network = connectivityManager.activeNetwork ?: return false
        val networkCapabilities =
            connectivityManager.getNetworkCapabilities(network) ?: return false

        return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
    }

    val isConnected = isWifiConnected(context)
    Log.e("internetcreate", "$isConnected")

    val galleryLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            viewModel.uploadImageToStorage(uri, onPostPhotoChange)
        }


    val nominatimApi = RetrofitHelper.getInstance().create(MyGeoApi::class.java)

    /////// CODIGO PARA OBTER PERMISSÃO DO USO DA LOCALIZAÇÃO
    val permission_given = remember {
        mutableStateOf(0)
    }

    if (ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        permission_given.value = 2
    }

    val permissionLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                permission_given.value += 1
            }
        }

    LaunchedEffect(key1 = "Permission") {
        permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        permissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    ///////// CASO TENHA PERMISSÃO, OBTEM AS COORDENADAS E REGISTA NO POST NO CAMPO LATITUDE E LONGITUDE
    if (permission_given.value == 2) {
        DisposableEffect(Unit) {
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    onPostLatitudeChange(location.latitude)
                    onPostLongitudeChange(location.longitude)
                } else {
                    onPostLatitudeChange(0.0)
                    onPostLongitudeChange(0.0)
                }

            }.addOnFailureListener {
                onPostLatitudeChange(0.0)
                onPostLongitudeChange(0.0)
            }
            val locationRequest = LocationRequest.Builder(
                Priority.PRIORITY_HIGH_ACCURACY,
                1000,
            ).setMinUpdateIntervalMillis(2000).build()

            val locationCallback = object : LocationCallback() {
                override fun onLocationResult(locations: LocationResult) {
                    for (location in locations.locations) {
                        onPostLatitudeChange(location.latitude)
                        onPostLongitudeChange(location.longitude)
                        nominatimApi.getGeoLocationInfo(location.latitude, location.longitude)
                            .enqueue(object : Callback<JsonObject> {
                                override fun onResponse(
                                    call: Call<JsonObject>,
                                    response: Response<JsonObject>
                                ) {
                                    val responseBodyLocation = response.body()
                                    responseBodyLocation?.let { json ->
                                        val postcode = json.getAsJsonObject("address")
                                            ?.get("postcode")?.asString

                                        onPostPostcodeChange(postcode.toString())
                                    }
                                }

                                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                                    Log.e("API Request", "Erro ao obter resposta: ${t.message}", t)
                                }
                            })
                    }
                }
            }

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
            onDispose { fusedLocationClient.removeLocationUpdates(locationCallback) }
        }

        /////////////////////////////////////////////////


        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            OutlinedTextField(
                value = postTitle,
                onValueChange = onPostTitleChange,
                label = { Text("Title") }
            )

            Spacer(modifier = Modifier.height(16.dp))

            OutlinedTextField(
                value = postDescription,
                onValueChange = onPostDescriptionChange,
                label = { Text("Description") }
            )

            Spacer(modifier = Modifier.height(16.dp))

            Button(onClick = {
                galleryLauncher.launch("image/*")
            }) {
                Text("Escolher Imagem da Galeria")
            }

            Spacer(modifier = Modifier.height(16.dp))

            if (postType != TypePost.CURIOSIDADE && postType != TypePost.DEFAULT) {
                OutlinedTextField(
                    value = postLifespan.toString(),
                    onValueChange = { onPostLifespanChange(it.toIntOrNull() ?: 0) },
                    label = { Text("lifespan (in days)") }
                )

            }


            Spacer(modifier = Modifier.height(16.dp))

            Text("Select Post Type:")
            Row(verticalAlignment = Alignment.CenterVertically) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    RadioButton(
                        selected = postType == TypePost.PERIGO,
                        onClick = { onPostTypeChange(TypePost.PERIGO) },
                        modifier = Modifier.padding(end = 8.dp)
                    )
                    Text("Perigo")
                }

                Spacer(modifier = Modifier.width(16.dp))

                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    RadioButton(
                        selected = postType == TypePost.CURIOSIDADE,
                        onClick = { onPostTypeChange(TypePost.CURIOSIDADE) },
                        modifier = Modifier.padding(horizontal = 8.dp)
                    )
                    Text("Curiosidades")
                }

                Spacer(modifier = Modifier.width(16.dp))

                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    RadioButton(
                        selected = postType == TypePost.EVENTO,
                        onClick = { onPostTypeChange(TypePost.EVENTO) },
                        modifier = Modifier.padding(start = 8.dp)
                    )
                    Text("Eventos")
                }
            }

            Spacer(modifier = Modifier.height(16.dp))

            Button(onClick = {

                if (postTitle.length > 20) {
                    Toast.makeText(
                        context,
                        "O título não deve ter mais de 20 caracteres",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (postTitle.isBlank()) {
                    Toast.makeText(
                        context,
                        "Por favor, insira um título",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (postDescription.isBlank()) {
                    Toast.makeText(
                        context,
                        "Por favor, insira uma descrição",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (postLifespan == null) {
                    Toast.makeText(
                        context,
                        "Por favor, insira um tempo de vida do post",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (postType == TypePost.DEFAULT) {
                    Toast.makeText(
                        context,
                        "Por favor, insira um tipo do post",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (postPostcode.isBlank()) {
                    Toast.makeText(
                        context,
                        "Por favor, espere até que possa ser guardado o código-postal",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (postPhoto.isBlank()) {
                    Toast.makeText(
                        context,
                        "Por favor, espere até que possa ser guardada a fotografia",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    if (postType == TypePost.CURIOSIDADE) {
                        onPostLifespanChange(9999)
                    } else if ((postType == TypePost.EVENTO || postType == TypePost.PERIGO) && postLifespan < 1) {
                        onPostLifespanChange(1)
                    } else if ((postType == TypePost.EVENTO || postType == TypePost.PERIGO) && postLifespan > 30) {
                        onPostLifespanChange(30)
                    }

                    if (!isConnected) {
                        onPostPhotoChange("")
                        Toast.makeText(
                            context,
                            "Não tem conexão com a internet. Fotografia escolhida não enviada.",
                            Toast.LENGTH_SHORT
                        ).show()
                        onPostPostcodeChange("")
                        Toast.makeText(
                            context,
                            "Não tem conexão com a internet. Código postal não obtido.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    val post = Post(
                        "",
                        postTitle,
                        postDescription,
                        user?.email,
                        postPhoto,
                        postPostcode,
                        postLatitude,
                        postLongitude,
                        postLifespan,
                        0,
                        listOf(),
                        0,
                        listOf(),
                        postType,
                        0,
                        true,
                        0.0.toFloat(),
                        listOf(),
                        listOf(),
                        listOf()
                    )
                    onAddClick(post)
                    navController.navigate("Home")
                }
            }) {
                Text("Enviar Post")
            }
        }
    }
}




