package pt.ipp.estg.cidadeinforma.screens

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import pt.ipp.estg.cidadeinforma.database.Post
import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.TextField
import androidx.core.content.edit
import pt.ipp.estg.cidadeinforma.viewModels.PostsViewModel
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale


/**
 * Composable que exibe os detalhes de um post, incluindo título, descrição, código postal, autor e data de criação.
 * @param post O objeto Post contendo as informações do post a ser exibido.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PostDetail(navController: NavController, postId: String) {
    val postsViewModel: PostsViewModel = viewModel()
    val user = Firebase.auth.currentUser
    val userId = Firebase.auth.currentUser?.uid
    val context = LocalContext.current
    val posts by postsViewModel.allPosts.observeAsState(emptyList())
    var comment by remember { mutableStateOf("") }

    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val isConnected = connectivityManager.activeNetworkInfo?.isConnectedOrConnecting == true

    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        item {
            val post = posts.find { it.id == postId }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                if (post != null && isConnected) {
                    Text(
                        text = post.title,
                        style = MaterialTheme.typography.titleLarge,
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentSize(Alignment.Center)
                    )
                    Text(
                        text = "${post.typePost}",
                        style = MaterialTheme.typography.titleSmall,
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentSize(Alignment.Center))
                    Spacer(modifier = Modifier.height(16.dp))
                    PostDetails(post = post)
                    if (!post.photo.equals("")) {
                        Spacer(modifier = Modifier.height(16.dp))
                        AsyncImage(
                            model = post.photo,
                            contentDescription = "Image from the post",
                            modifier = Modifier
                                .fillMaxWidth()
                                .size(width = 150.dp, height = 150.dp)
                                .padding(top = 16.dp)
                        )
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        RatingStars(
                            currentRating = post.rating,
                            post = post,
                            userId = userId,
                            onRatingChanged = { newRating ->
                                if (userId != null) {
                                    postsViewModel.ratePost(postId, userId, newRating, post)
                                    if (!post.rating_users.contains(userId)) {
                                        Toast.makeText(
                                            context,
                                            "Você atribuiu um rating de ${newRating.toInt()} estrelas",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    } else {
                                        Toast.makeText(
                                            context,
                                            "Você já atribuiu um rating a este post",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                        )
                    }

                    Spacer(modifier = Modifier.height(8.dp))

                    val mediaRating = String.format("%.1f", post.rating / (post.rating_users.size - 1))
                    Text(
                        text = "A média de rating deste post é $mediaRating",
                        style = MaterialTheme.typography.bodySmall,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(16.dp))

                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Button(
                            onClick = {
                                if (userId != null) {
                                    postsViewModel.upvotePost(postId, userId, post)
                                }
                            }
                        ) {
                            Text(
                                text = "Upvote (${post.upvotes})",
                                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
                            )
                        }
                        Spacer(modifier = Modifier.width(8.dp))
                        Button(
                            onClick = {
                                if (userId != null) {
                                    postsViewModel.downvotePost(postId, userId, post)
                                }
                            }
                        ) {
                            Text(
                                text = "Downvote (${post.downvotes})",
                                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(35.dp))
                    TextField(
                        value = comment,
                        onValueChange = { comment = it },
                        modifier = Modifier.fillMaxWidth()
                            .padding(vertical = 8.dp, horizontal = 16.dp),
                        placeholder = { Text(text = "Digite seu comentário aqui...") }
                    )
                    Button(
                        onClick = {
                            if (userId != null) {
                                if(comment.equals("")){
                                    Toast.makeText(context, "Escreva alguma coisa no comentário", Toast.LENGTH_SHORT).show()
                                } else {
                                    postsViewModel.addCommentToPost(comment, userId, postId, user, post)
                                    comment = ""
                                }
                            }
                        },
                        modifier = Modifier.align(Alignment.End)
                    ) {
                        Text(text = "Enviar")
                    }
                    Spacer(modifier = Modifier.height(8.dp))
                    post.comments.forEach { commentString ->
                        val parts = commentString.split("/%/%/")
                        val userComment = parts.first()
                        val userEmail = parts.last()
                        Log.e("split", "$userComment , $userEmail")
                        Column(modifier = Modifier.padding(8.dp)) {
                            Text(text = userEmail, fontWeight = FontWeight.Bold)
                            Text(text = userComment)
                        }
                    }
                } else if (post != null && !isConnected) {
                    Text(
                        text = post.title,
                        style = MaterialTheme.typography.bodyLarge,
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentSize(Alignment.Center)
                    )
                    Text(
                        text = "${post.typePost}",
                        style = MaterialTheme.typography.titleSmall,
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentSize(Alignment.Center))
                    Spacer(modifier = Modifier.height(16.dp))
                    PostDetails(post = post)
                    Spacer(modifier = Modifier.height(16.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        RatingStars(
                            currentRating = post.rating,
                            post = post,
                            userId = userId,
                            onRatingChanged = {
                                Toast.makeText(context, "Você não está conectado à internet", Toast.LENGTH_SHORT).show()
                            }
                        )
                    }

                    Spacer(modifier = Modifier.height(16.dp))

                    val mediaRating = String.format("%.1f", post.rating / (post.rating_users.size - 1))
                    if (post.rating_users.isEmpty()) {
                        Text(
                            text = "A média de rating deste post é 0",
                            style = MaterialTheme.typography.bodySmall,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth()
                        )
                    } else {
                        Text(
                            text = "A média de rating deste post é $mediaRating",
                            style = MaterialTheme.typography.bodySmall,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth()
                        )
                    }

                    Spacer(modifier = Modifier.height(16.dp))

                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Button(
                            onClick = {
                                Toast.makeText(context, "Você não está conectado à internet", Toast.LENGTH_SHORT).show()
                            }
                        ) {
                            Text(
                                text = "Upvote (${post.upvotes})",
                                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
                            )
                        }
                        Spacer(modifier = Modifier.width(8.dp))
                        Button(
                            onClick = {
                                Toast.makeText(context, "Você não está conectado à internet", Toast.LENGTH_SHORT).show()
                            }
                        ) {
                            Text(
                                text = "Downvote (${post.downvotes})",
                                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(22.dp))
                    TextField(
                        value = comment,
                        onValueChange = { comment = it },
                        modifier = Modifier.fillMaxWidth()
                            .padding(vertical = 8.dp, horizontal = 16.dp),
                        placeholder = { Text(text = "Digite seu comentário aqui...") }
                    )
                    Button(
                        onClick = {
                            Toast.makeText(context, "Você não está conectado à internet", Toast.LENGTH_SHORT).show()
                        },
                        modifier = Modifier.align(Alignment.End)
                    ) {
                        Text(text = "Enviar")
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    post.comments.forEach { commentString ->
                        val parts = commentString.split("/%/%/")
                        val userComment = parts.first()
                        val userEmail = parts.last()
                        Log.e("split", "$userComment , $userEmail")
                        Column(modifier = Modifier.padding(8.dp)) {
                            Text(text = userEmail, fontWeight = FontWeight.Bold)
                            Text(text = userComment)
                        }
                    }
                }
            }
        }
    }
}




@Composable
fun PostDetails(post: Post) {
    val context = LocalContext.current
    Column {
        Text(text = "${post.description}", style = MaterialTheme.typography.bodyLarge)
        Spacer(modifier = Modifier.height(32.dp))
        if (post.postcode.equals("")) {
            Text(text = "Código Postal: Indisponível", style = MaterialTheme.typography.bodySmall)
            Spacer(modifier = Modifier.height(8.dp))
        } else {
            Text(text = "Código Postal: ${post.postcode}", style = MaterialTheme.typography.bodySmall)
            Spacer(modifier = Modifier.height(8.dp))
        }
        Text(text = "Autor: ${post.author}", style = MaterialTheme.typography.bodySmall)
        val dateformatted = getFormattedDateFromMillis(post.createdAt, context)
        Spacer(modifier = Modifier.height(8.dp))
        Text(text = "Criado a ${dateformatted}", style = MaterialTheme.typography.bodySmall)
    }
}

/**
 * Função auxiliar para formatar uma data e hora a partir de milissegundos.
 * @param millis O tempo em milissegundos a ser formatado.
 * @param context O contexto atual.
 * @return Uma string formatada representando a data e hora.
 */
private fun getFormattedDateFromMillis(millis: Long, context: Context): String {
    val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = millis
    return dateFormat.format(calendar.time)
}


/**
 * Composable que exibe um conjunto de ícones de estrelas para permitir ao utilizador atribuir um rating a um post.
 * @param currentRating A classificação atual do post.
 * @param post O objeto Post associado a este conjunto de estrelas.
 * @param userId O ID do utilizador atualmente logado.
 * @param onRatingChanged Função de callback chamada quando o rating é alterado pelo utilizador.
 */
@Composable
fun RatingStars(
    currentRating: Float,
    post: Post,
    userId: String?,
    onRatingChanged: (Float) -> Unit
) {
    val selectedColor = Color.Blue
    val unselectedColor = Color.Gray

    val filledStars = if (userId != null && post.rating_users.contains(userId)) {
        ((currentRating) / (post.rating_users.size - 1)).toInt()
    } else {
        0 // Nenhuma estrela preenchida se o utilizador não tiver votado
    }

    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        (1..5).forEach { index ->
            val isSelected = index <= filledStars

            IconButton(
                onClick = { onRatingChanged(index.toFloat()) },
                modifier = Modifier.padding(4.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Star,
                    contentDescription = null,
                    tint = if (isSelected) selectedColor else unselectedColor
                )
            }
        }
    }
}
