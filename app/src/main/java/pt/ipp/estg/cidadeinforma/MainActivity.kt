package pt.ipp.estg.cidadeinforma

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import pt.ipp.estg.cidadeinforma.ui.theme.CidadeInformaTheme
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import pt.ipp.estg.cidadeinforma.screens.HomeScreen
import pt.ipp.estg.cidadeinforma.screens.SignInScreen
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import pt.ipp.estg.cidadeinforma.navigation.MyNavigatonDrawer
import pt.ipp.estg.cidadeinforma.ui.theme.NavigationScaffoldNavigationDrawerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NavigationScaffoldNavigationDrawerTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyNavigatonDrawer()
                }
            }
        }
    }
}