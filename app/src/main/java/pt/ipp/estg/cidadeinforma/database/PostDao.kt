package pt.ipp.estg.cidadeinforma.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface PostDao {

    @Query("select * from Post")
    fun getPosts(): LiveData<List<Post>>

    @Query("select * from Post where Id = :id")
    fun getOnePost(id:Int): LiveData<Post>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(post:Post) : Long

    @Update
    suspend fun update(post:Post)

    @Delete
    suspend fun delete(post:Post) : Int

    @Query("DELETE FROM Post WHERE Id = :postId")
    suspend fun deleteById(postId: String)
}
