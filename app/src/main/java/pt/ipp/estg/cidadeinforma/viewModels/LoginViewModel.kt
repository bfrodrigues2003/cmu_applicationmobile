package pt.ipp.estg.cidadeinforma.viewModels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await


/**
 * ViewModel responsável por gerir a lógica de autenticação do utilizador.
 * Gere o estado de autenticação e realiza operações de registo, login e logout.
 *
 * @param application O contexto da aplicação.
 */
class LoginViewModel(application: Application): AndroidViewModel(application) {
    val authState : MutableLiveData<AuthStatus>
    val fAuth : FirebaseAuth

    // Inicialização da ViewModel
    init {
        authState = MutableLiveData(AuthStatus.NOLOGGIN)
        fAuth = Firebase.auth
    }

    /**
     * Função para registar um novo utilizador com o email e senha fornecidos.
     * Atualiza o estado de autenticação e regista o utilizador no Firebase Auth.
     *
     * @param email Email do utilizador.
     * @param password Senha do utilizador.
     */
    fun register(email:String, password:String){
        viewModelScope.launch {
            try{
                val result = fAuth.createUserWithEmailAndPassword(email, password).await()
                if (result != null && result.user != null){
                    authState.postValue(AuthStatus.LOGGED)
                    Log.d("Register","logged in")
                    return@launch
                }
                Log.d("Register","anonymous")
                authState.postValue(AuthStatus.NOLOGGIN)
                return@launch
            } catch( e:Exception) {}
        }
    }

    /**
     * Função para realizar o login com o email e senha fornecidos.
     * Atualiza o estado de autenticação e realiza o login no Firebase Auth.
     *
     * @param email Email do utilizador.
     * @param password Senha do utilizador.
     */
    fun login(email:String, password:String){
        viewModelScope.launch {
            try{
                val result = fAuth.signInWithEmailAndPassword(email, password).await()
                if (result != null && result.user != null){
                    authState.postValue(AuthStatus.LOGGED)
                    Log.d("Login","logged in")
                    return@launch
                }
                Log.d("Login","anonymous")
                authState.postValue(AuthStatus.NOLOGGIN)
                return@launch
            } catch( e:Exception) {}
        }
    }

    /**
     * Função para realizar o logout do utilizador.
     * Atualiza o estado de autenticação para "não logado" e faz logout no Firebase Auth.
     *
     * @param navController Controlador de navegação para redirecionar o utilizador após o logout.
     */
    fun logout(
        navController: NavController
    ){
        viewModelScope.launch {
            fAuth.signOut()
            authState.postValue(AuthStatus.NOLOGGIN)
            Log.d("Login","logout")
            navController.navigate("Home")
        }
    }

    // Enumeração que representa os possíveis estados de autenticação
    enum class AuthStatus {
        LOGGED, NOLOGGIN
    }

}