package pt.ipp.estg.cidadeinforma.database

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [Post::class], version = 6)
@TypeConverters(ListStringConverter::class)
abstract class PostDatabase : RoomDatabase() {

    abstract fun getPostDao():PostDao

    companion object{
        private var INSTANCE:PostDatabase?=null

        fun getDatabase(context: Context):PostDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    PostDatabase::class.java,
                    "post-database"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}
