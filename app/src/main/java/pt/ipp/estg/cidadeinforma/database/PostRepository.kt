package pt.ipp.estg.cidadeinforma.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class PostRepository(val postDao: PostDao) {

    fun getPosts(): LiveData<List<Post>> {
        return postDao.getPosts()
    }

    fun getPost(id:Int): LiveData<Post> {
        return postDao.getOnePost(id)
    }

    fun insert(post:Post){
        postDao.insert(post)
    }

    suspend fun update(post:Post){
        postDao.update(post)
    }

    suspend fun delete(post: Post){
        postDao.delete(post)
    }

    suspend fun deleteById(postId: String) {
        postDao.deleteById(postId)
    }
}
