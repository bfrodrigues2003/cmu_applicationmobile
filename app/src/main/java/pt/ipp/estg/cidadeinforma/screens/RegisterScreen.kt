package pt.ipp.estg.cidadeinforma.screens

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import pt.ipp.estg.cidadeinforma.R
import pt.ipp.estg.cidadeinforma.viewModels.LoginViewModel

/**
 * Composable que representa a tela de registo da aplicação.
 * Exibe um logotipo da aplicação, campos de entrada para email e senha, um botão de registo e um link para fazer login.
 * Redireciona o utilizador para a tela de perfil se já estiver autenticado.
 *
 * @param navController Controlador de navegação para gerir a navegação entre screen.
 */
@Composable
fun RegisterScreen(navController: NavController) {
    val email = rememberSaveable { mutableStateOf("") }
    val password = rememberSaveable { mutableStateOf("") }
    val viewModel: LoginViewModel = viewModel()
    val authStatus = viewModel.authState.observeAsState()
    val user = Firebase.auth.currentUser


    Box(
        Modifier
            .background(MaterialTheme.colorScheme.surface)
            .fillMaxSize()) {
        Column(
            Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally){
            Image(
                painter = painterResource(id = R.drawable.icone_app),
                contentDescription = "Logotipo da aplicação",
                modifier = Modifier
                    .size(225.dp)
                    .padding(top = 50.dp)
            )
            Spacer(Modifier.height(15.dp))
            Text("Bem-vindo!",
                fontSize = 24.sp)
        }
    }

    if (user == null) {
        RegisterFields(navController, email = email.value, password = password.value,
            onEmailChange = { email.value = it },
            onPasswordChange = { password.value = it },
            onRegisterClick = { viewModel.register(email.value, password.value) }
        )
        return
    } else {
        navController.navigate("Profile")
    }

}

/**
 * Composable que representa os campos de entrada e a lógica para o registro de um novo utilizador.
 *
 * @param navController Controlador de navegação para gerir a navegação entre screens.
 * @param email Email inserido pelo utilizador.
 * @param password Senha inserido pelo utilizador.
 * @param onEmailChange Função de callback chamada quando o email é alterado.
 * @param onPasswordChange Função de callback chamada quando a senha é alterada.
 * @param onRegisterClick Função de callback chamada quando o botão de registo é clicado.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterFields(
    navController: NavController,
    email: String,
    password: String,
    onEmailChange: (String) -> Unit,
    onPasswordChange: (String) -> Unit,
    onRegisterClick: (String) -> Unit
) {
    val context = LocalContext.current

    var passwordRepeat by rememberSaveable { mutableStateOf("") }

    Column(
        modifier = Modifier
            .padding(top = 300.dp)
            .fillMaxWidth()
            .height(300.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Registe-se")
        OutlinedTextField(
            value = email,
            placeholder = { Text(text = "user@email.com") },
            label = { Text(text = "email") },
            onValueChange = onEmailChange,
        )
        OutlinedTextField(
            value = password,
            placeholder = { Text(text = "password") },
            label = { Text(text = "password") },
            onValueChange = onPasswordChange,
            visualTransformation = PasswordVisualTransformation()
        )
        OutlinedTextField(
            value = passwordRepeat,
            placeholder = { Text(text = "repita a password") },
            label = { Text(text = "repita a password") },
            onValueChange = { passwordRepeat = it },
            visualTransformation = PasswordVisualTransformation()
        )

        Button(onClick = {
            if (email.isNotBlank() && password.isNotBlank() && password == passwordRepeat) {
                onRegisterClick(email)
                navController.navigate("Home")
            } else {
                if (password != passwordRepeat) {
                    Toast.makeText(
                        context,
                        "As senhas não coincidem",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        context,
                        "Por favor insira um email e uma password",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }) {
            Text("Registo")
        }
        ClickableText(
            text = buildAnnotatedString {
                append("Já possui uma conta? Faça login!")
                addStyle(
                    style = SpanStyle(textDecoration = TextDecoration.Underline),
                    start = 0,
                    end = 32
                )
            },
            onClick = {  navController.navigate("Profile")
            },
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(bottom = 16.dp)
        )
    }
}