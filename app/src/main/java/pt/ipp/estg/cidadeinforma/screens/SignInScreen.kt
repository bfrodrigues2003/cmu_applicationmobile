package pt.ipp.estg.cidadeinforma.screens

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import pt.ipp.estg.cidadeinforma.viewModels.LoginViewModel
import pt.ipp.estg.cidadeinforma.R

/**
 * Composable que representa a tela de login da aplicação.
 * Exibe um logotipo da aplicação, campos de entrada para email e senha, um botão de login e um link para o registo.
 * Redireciona o utilizador para a tela inicial se já estiver autenticado.
 *
 * @param navController Controlador de navegação para gerir a navegação entre screens.
 */
@Composable
fun SignInScreen(navController:NavController) {
    val email = rememberSaveable { mutableStateOf("") }
    val password = rememberSaveable { mutableStateOf("") }
    val viewModel: LoginViewModel = viewModel()
    val authStatus = viewModel.authState.observeAsState()
    val user = Firebase.auth.currentUser


    Box(
        Modifier
            .background(MaterialTheme.colorScheme.surface)
            .fillMaxSize()) {
        Column(Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally){
            Image(
                painter = painterResource(id = R.drawable.icone_app),
                contentDescription = "Logotipo da aplicação",
                modifier = Modifier
                    .size(225.dp)
                    .padding(top = 50.dp)
            )
            Spacer(Modifier.height(15.dp))
            Text("Bem-vindo!",
                fontSize = 24.sp)
        }
    }

    if (user == null) {
        LoginFields(navController, email = email.value, password = password.value,
            onEmailChange = { email.value = it },
            onPasswordChange = { password.value = it },
            onLoginClick = { viewModel.login(email.value, password.value) }
        )
        return
    } else {
        LogoutFields() {
            viewModel.logout(navController)
        }
    }

}


/**
 * Composable que representa os campos de entrada e a lógica para o login do utilizador.
 *
 * @param navController Controlador de navegação para gerenciar a navegação entre telas.
 * @param email Email inserida pelo utilizador.
 * @param password Senha inserida pelo usuário.
 * @param onEmailChange Função de callback chamada quando o email é alterado.
 * @param onPasswordChange Função de callback chamada quando a senha é alterada.
 * @param onLoginClick Função de callback chamada quando o botão de login é clicado.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginFields(
    navController: NavController,
    email: String,
    password: String,
    onEmailChange: (String) -> Unit,
    onPasswordChange: (String) -> Unit,
    onLoginClick: (String) -> Unit
) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .padding(top = 250.dp)
            .fillMaxWidth()
            .height(300.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Insira os seus dados")
        OutlinedTextField(
            value = email,
            placeholder = { Text(text = "user@email.com") },
            label = { Text(text = "email") },
            onValueChange = onEmailChange,
        )
        OutlinedTextField(
            value = password,
            placeholder = { Text(text = "password") },
            label = { Text(text = "password") },
            onValueChange = onPasswordChange,
            visualTransformation = PasswordVisualTransformation()
        )

        Button(onClick = {
            if (email.isBlank() == false && password.isBlank() == false) {
                onLoginClick(email)
                navController.navigate("Home")
            } else {
                Toast.makeText(
                    context,
                    "Por favor insira um email e uma password",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }) {
            Text("Login")
        }
        ClickableText(
            text = buildAnnotatedString {
                append("Não possui uma conta? Registe-se!")
                addStyle(
                    style = SpanStyle(textDecoration = TextDecoration.Underline),
                    start = 0,
                    end = 33
                )
            },
            onClick = {navController.navigate("Register")
            },
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(bottom = 16.dp)
        )
    }
}

/**
 * Composable que representa a secção de logout da tela de login.
 *
 * @param onLogoutClick Função de callback chamada quando o botão de logout é clicado.
 */
@Composable
fun LogoutFields(
    onLogoutClick: () -> Unit
) {
    val context = LocalContext.current
    val user = Firebase.auth.currentUser

    Column(
        modifier = Modifier
            .padding(top = 250.dp)
            .fillMaxWidth()
            .height(300.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (user != null) {
            Text(
                text = "Você está autenticado como",
                textAlign = TextAlign.Center,
                fontSize = 16.sp,
                modifier = Modifier.padding(bottom = 8.dp)
            )
            Text(
                text = "${user.email}",
                textAlign = TextAlign.Center,
                fontSize = 16.sp,
                modifier = Modifier.padding(bottom = 8.dp)
            )
        }
        Button(onClick = onLogoutClick) {
            Text("Logout")
        }

    }
}


