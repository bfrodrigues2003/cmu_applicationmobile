package pt.ipp.estg.cidadeinforma.screens

import android.Manifest
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import pt.ipp.estg.cidadeinforma.R
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.core.app.NotificationCompat
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import pt.ipp.estg.cidadeinforma.database.Post
import pt.ipp.estg.cidadeinforma.viewModels.PostsViewModel
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.pm.PackageManager
import android.location.Location
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.material.icons.rounded.Favorite
import androidx.compose.material3.TabRowDefaults.tabIndicatorOffset
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

/**
 * variável que será responsável por atribuir um id à notificação
 */
var notificationId = 0


/**
 * Função principal da página inicial que vai chamar as demais funções para atribuir valor
 * e completar a screen
 * @param navController navController responsável por fazer navegar entre screens
 */
@Composable
fun HomeScreen(navController: NavController)
{
    val user = Firebase.auth.currentUser
    val context = LocalContext.current
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val viewModel: PostsViewModel =  viewModel()
    val list by viewModel.allPosts.observeAsState(emptyList())
    var upvotedPosts by remember { mutableStateOf(listOf<Post>()) }
    val CHANNEL_ID = "EventChannel"
    val channel_name = "Acontecimentos"
    val channel_description = "Aqui ficam as notificações que têm como conteúdo notificação de acontecimentos"
    var fusedLocationClient: FusedLocationProviderClient
    var selectedIndex by remember { mutableStateOf(0) }


    val isConnected = connectivityManager.activeNetworkInfo?.isConnectedOrConnecting == true
    Log.e("internethome", "$isConnected")
    Log.e("postsroom", "$list")

    fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    val permission_given = remember {
        mutableStateOf(0)
    }


    ///////////////////// TRECHO DE CODIGO PARA ENVIAR NOTIFICAÇÕES ////////////////////////////
    if (ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        permission_given.value = 2
    }

    val permissionLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                permission_given.value += 1
            }
        }

    LaunchedEffect(key1 = "Permission") {
        permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        permissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    if (permission_given.value == 2) {
        Log.e("etapa1", "entrou")
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    Log.e("etapa2", "entrou")
                    for (post in list) {
                        Log.e("etapa3", "entrou")
                        val postLocation = Location("").apply {
                            latitude = post.latitude
                            longitude = post.longitude
                        }
                        val distance = location.distanceTo(postLocation)
                        if (distance < 100) {
                            Log.e("etapa4", "entrou")
                            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                                .setSmallIcon(R.drawable.news1)
                                .setContentTitle("Você está perto de um post!")
                                .setContentText("\"${post.title}\" está a menos de 100 metros de um post.")
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                val name = channel_name
                                val descriptionText = channel_description
                                val importance = NotificationManager.IMPORTANCE_DEFAULT
                                val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                                    description = descriptionText
                                }
                                val notificationManager: NotificationManager =
                                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                notificationManager.createNotificationChannel(channel)
                            }


                            with(NotificationManagerCompat.from(context)) {
                                Log.e("etapa5", "entrou")
                                notify(notificationId++, builder.build())
                            }
                        }
                    }
                }
            }
    }
    ///////////////////////////////////////////////////////////////////////

    Box(
        Modifier
            .background(MaterialTheme.colorScheme.background)
            .fillMaxSize()
    ) {
        LazyColumn {
            item {
                TopAppBar()
            }
            item {
                NavBar(selectedIndex) { newIndex ->
                    selectedIndex = newIndex
                    if (newIndex == 1) {
                        upvotedPosts = list.filter { it.upvotes_users.contains(user?.uid) }
                    }
                }
            }
            item {
                NewPostButtonBar(navController, user)
            }
            items(if (selectedIndex == 1) upvotedPosts else list) { post ->
                if (post.visible) {
                    PostItem(navController, post = post)
                }
            }
        }
    }

}

/**
 * Função responsável por apresentar a imagem e o texto com o nome da aplicação
 */
@Composable
private fun TopAppBar() {
    Surface {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    painter = painterResource(id = R.drawable.city_image),
                    contentDescription = "Imagem a representar uma cidade para efeitos visuais",
                    modifier = Modifier
                        .size(150.dp)
                        .padding(top = 16.dp, bottom = 16.dp)
                )
                Text(
                    text = "Cidade Informa",
                    style = TextStyle(
                        fontSize = 30.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            }
        }
    }
}


/**
 * Atribui uma barra de navegação pequena que possibilita o utilizador a navegar entre
 * posts totais e posts em que deu like
 * @param selectedIndex index escolhido (opção da barra escolhida)
 * @param onIndexChanged comportamento tomado quando o index é alterado
 */
@Composable
fun NavBar(selectedIndex: Int, onIndexChanged: (Int) -> Unit) {
    Surface {
        TabRow(
            selectedTabIndex = selectedIndex,
            indicator = { tabPositions ->
                TabRowDefaults.Indicator(
                    color = Color.Black,
                    modifier = Modifier.tabIndicatorOffset(tabPositions[selectedIndex])
                )
            }
        ) {
            Tab(
                selected = selectedIndex == 0,
                onClick = { onIndexChanged(0) }
            ) {
                Icon(
                    Icons.Rounded.Home,
                    contentDescription = "Home screen com todos os posts",
                    modifier = Modifier.padding(top = 4.dp, bottom = 6.dp)
                )
            }
            Tab(
                selected = selectedIndex == 1,
                onClick = { onIndexChanged(1) }
            ) {
                Icon(
                    Icons.Rounded.Favorite,
                    contentDescription = "Tab para os posts que receberam like do user",
                    modifier = Modifier.padding(top = 4.dp, bottom = 6.dp)
                )
            }
        }
    }
}


/**
 * Composable que exibe um botão para criar um novo post. Se o usuário estiver logado,
 * ao clicar no botão, ele será redirecionado para a tela de criação de post (`Createpost`).
 * Se o usuário não estiver logado, será redirecionado para a tela de perfil (`Profile`).
 *
 * @param navController NavController usado para navegar para diferentes telas no aplicativo.
 * @param user Objeto FirebaseUser representando o usuário atualmente logado.
 */
@Composable
fun NewPostButtonBar(
    navController:NavController,
    user:FirebaseUser?
) {
    Surface {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier.weight(1f))
            Button(
                onClick = {
                    if (user != null) {
                        navController.navigate("Createpost")
                    } else {
                        navController.navigate("Profile")
                    }
                }
            ) {
                Text("Criar Post")
            }
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}


/**
 * Composable que exibe um post. O cartão exibe o título do post,
 * o tipo de post e um botão para ver mais detalhes do post.
 *
 * @param navController NavController usado para navegar para a tela de detalhes do post.
 * @param post Objeto Post representando as informações do post a ser exibido.
 */
@Composable
fun PostItem(
    navController: NavController,
    post: Post,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp, horizontal = 16.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = post.title,
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
            )
            Text(
                text = post.typePost.toString(),
                style = TextStyle(
                    fontSize = 14.sp,
                    color = Color.Gray,
                    textAlign = TextAlign.Center
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
            )
            Spacer(modifier = Modifier.weight(1f))
            Button(
                onClick = { navController.navigate("PostDetail/${post.id}") },
                modifier = Modifier.align(Alignment.CenterHorizontally)
            ) {
                Text("Ver")
            }
        }
    }
}


