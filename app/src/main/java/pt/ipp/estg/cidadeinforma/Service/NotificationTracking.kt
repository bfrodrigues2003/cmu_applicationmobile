package pt.ipp.estg.cidadeinforma.Service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import androidx.core.app.NotificationCompat
import pt.ipp.estg.cidadeinforma.R
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.widget.Toast
import pt.ipp.estg.cidadeinforma.database.Post
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

/*class NotificationTracking : Service() {

    private val CHANNEL_ID = "EventChannel"
    private val FOREGROUND_ID = 1

    private var operation = "BACKGROUND"

    private var notfId = 0

    companion object{
        var SERVICE_RUNNING = false
    }

    private val broadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                it.getStringExtra("MODE")?.let {
                    if(it.equals("BACKGROUND")){
                        stopServiceForeGround()
                    }
                }
                it.getStringExtra("GET")?.let {
                    if (it.equals("STATUS")) {
                        val intent = Intent("pt.ipp.estg.MyService")
                        intent.putExtra("MODE",operation)
                        sendBroadcast(intent)
                    }
                }
            }
        }
    }


    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
        val intentFilter = IntentFilter()
        intentFilter.addAction("pt.ipp.estg.MainActivity")
        registerReceiver(broadcastReceiver, intentFilter)
        SERVICE_RUNNING = true
    }


    // Método chamado quando o serviço é iniciado
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val context = intent?.getSerializableExtra("CONTEXT") as? Context
        val userLocation = intent?.getParcelableExtra<Location>("USER_LOCATION")
        val allPosts = intent?.getParcelableArrayListExtra<Post>("ALL_POSTS")

        context?.let {
            userLocation?.let { location ->
                allPosts?.let { posts ->
                    sendNotification(context, location, posts)
                    startLocationUpdates(context)
                }
            }
        }

        intent?.getStringExtra("MODE")?.let {
            if(it.equals("FOREGROUND")){
                startServiceInForeground()
            }
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        //TODO
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        SERVICE_RUNNING = false
    }

    fun createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun startServiceInForeground(){
        val not = Notification.Builder(this, CHANNEL_ID)
            .setContentTitle("service running")
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentText("executing tasks in background")
            .build()
        startForeground(FOREGROUND_ID,not)
        Log.d("tag", "service in foreground mode")
        this.operation = "FOREGROUND"
    }

    fun stopServiceForeGround(){
        stopForeground(Service.STOP_FOREGROUND_REMOVE)

        val intent = Intent("pt.ipp.estg.MyService")
        intent.putExtra("MODE","BACKGROUND")
        sendBroadcast(intent)

        Log.d("tag", "service is not in foreground mode")

        this.operation = "BACKGROUND"
    }

    fun looperInMainThread(){
        val handler = Handler(Looper.getMainLooper());
        val workToRun = Runnable {
            var i = 0
            while(i<100){
                i++
                Thread.sleep(1000)
                Toast.makeText(applicationContext, "doing work "+i, Toast.LENGTH_SHORT).show()
                Log.d("tag", "service running")
            }
        }
        handler.post(workToRun)
    }


    // Método para iniciar o rastreamento de localização
    private fun startLocationUpdates(context: Context) {
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient()
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            location?.let {
                // Quando a localização do usuário estiver disponível, verificar a proximidade dos posts
                sendNotification(this, location, viewModel.allPosts.value ?: emptyList())
            }
        }
    }



    // Método para verificar se um post está próximo ao usuário
    private fun isPostNearby(userLocation: Location, postLocation: Location): Boolean {
        // Lógica para calcular a distância entre as coordenadas
        // e verificar se está dentro do raio de 100 metros
        // ...
        return distance < 100
    }

    // Método para enviar a notificação
    private fun sendNotification(context: Context, userLocation: Location, allPosts: List<Post>) {
        val nearbyPosts = mutableListOf<Post>()

        for (post in allPosts) {
            val postLocation = Location("")
            postLocation.latitude = post.latitude
            postLocation.longitude = post.longitude

            // Calcular a distância entre o usuário e o post
            val distance = userLocation.distanceTo(postLocation)

            // Verificar se o post está a menos de 100 metros do usuário
            if (distance < 100) {
                nearbyPosts.add(post)
            }
        }

        for (post in nearbyPosts) {
            val textTitle = "Acontecimento Próximo"
            val textContent = "\"${post.title}\" está a menos de 100 metros de si!"

            // Construir a notificação
            var builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.news1)
                .setContentTitle(textTitle)
                .setContentText(textContent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            // Obter o NotificationManager e enviar a notificação
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(notfId++, builder.build())
        }




    }


}*/