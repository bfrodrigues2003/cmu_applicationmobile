package pt.ipp.estg.cidadeinforma.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Post(
    /**
     * chave primaria da base de dados, id de cada post
     */
    @PrimaryKey
    val id:String,
    val title:String,
    val description:String,
    val author:String?,
    val photo: String,
    val postcode: String,
    val latitude: Double,
    val longitude: Double,
    val lifespan:Int,
    val upvotes:Int,
    val upvotes_users: List<String>,
    val downvotes:Int,
    val downvotes_users: List<String>,
    val typePost: TypePost,
    val createdAt: Long,
    val visible: Boolean,
    val rating: Float,
    val rating_users: List<String>,
    val comments: List<String>,
    val comments_users: List<String>
    ) {
    constructor() : this("", "", "", "", "",
        "",0.0, 0.0, 1, 0, listOf(),
        0, listOf(), TypePost.CURIOSIDADE, 0, true,
        0.0.toFloat(), listOf(), listOf(), listOf())
}

/**
 * Enumeração para o tipo que cada post pode tomar
 */
enum class TypePost {
    CURIOSIDADE,
    PERIGO,
    EVENTO,
    DEFAULT
}
