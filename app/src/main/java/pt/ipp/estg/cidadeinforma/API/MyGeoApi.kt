package pt.ipp.estg.cidadeinforma.API

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface MyGeoApi {

    @GET("reverse")
    fun getGeoLocationInfo(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("zoom") zoom: Int=17,
        @Query("format") format: String = "json",
        @Query("addressdetails") addressdetails: Int=1
    ): Call<JsonObject>
}