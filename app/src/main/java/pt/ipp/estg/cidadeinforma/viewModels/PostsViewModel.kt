package pt.ipp.estg.cidadeinforma.viewModels

import kotlinx.coroutines.Dispatchers

import android.app.Application
import android.content.ContentValues.TAG
import android.net.Uri
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import pt.ipp.estg.cidadeinforma.database.Post
import pt.ipp.estg.cidadeinforma.database.PostDatabase
import pt.ipp.estg.cidadeinforma.database.PostRepository
import pt.ipp.estg.cidadeinforma.database.TypePost
import java.util.Calendar
import java.util.UUID

/**
 * ViewModel responsável por gerir os posts na aplicação.
 * Gere a obtenção, inserção, atualização e exclusão de posts.
 *
 * @param application O contexto da aplicação.
 */
class PostsViewModel(application: Application) : AndroidViewModel(application) {

    val repository: PostRepository
    val allPosts: LiveData<List<Post>>
    
    val db : FirebaseFirestore
    val collectionName : String
    val listPosts : MutableLiveData<List<Post>>

    // Inicialização da ViewModel
    init {
        db = Firebase.firestore
        collectionName = "POSTS"
        listPosts = MutableLiveData(listOf())
        val dbroom = PostDatabase.getDatabase(application)
        repository = PostRepository(dbroom.getPostDao())
        allPosts = repository.getPosts()

        // Obtém os posts em tempo real do Firestore e atualiza a lista de posts
        getPostsLive { posts ->
            listPosts.postValue(posts)
        }

        // Sincroniza os dados entre a base de dados local e o Firestore e ainda
        // altera esconde os posts caso estes tenham esgotado o seu tempo de vida
        viewModelScope.launch {
            synchronizeData()
            checkAndHideExpiredPosts()
        }

        // Escuta os deletes de posts no Firestore
        listenForPostDeletions()


    }

    /**
     * Insere um novo post na base de dados local.
     *
     * @param post O post a ser inserido.
     */
    fun insertPost(post: Post) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(post)
        }
    }

    /**
     * Atualiza um post na base de dados local.
     *
     * @param post O post atualizado.
     */
    fun updatedPost(post: Post) {
        viewModelScope.launch {
            repository.update(post)
        }
    }

    /**
     * Obtém um único post da base de dados local.
     *
     * @param id O ID do post a ser obtido.
     * @return Um LiveData contendo o post.
     */
    suspend fun getOnePost(id: Int): LiveData<Post> {
        return repository.getPost(id)
    }


    /**
     * Guarda um post no Firestore.
     *
     * @param post O post a ser guardado.
     */
    fun savePost(post: Post) {
        viewModelScope.launch {
            val postToSave = hashMapOf(
                "post" to post
            )
            val documentReference = db.collection(collectionName).add(postToSave)
            documentReference.addOnSuccessListener { documentReference ->
                val postId = documentReference.id
                Log.d(TAG, "Post saved successfully with ID: $postId")
                val updatedPost = post.copy(id = post.id)
                val postToSave = hashMapOf(
                    "post" to updatedPost
                )
                db.collection(collectionName)
                    .document(post.id)
                    .set(postToSave)
                    .addOnSuccessListener {
                        Log.d(TAG, "Updated post with ID $postId in Firestore")

                        // Exclui o documento anterior com o id errado
                        db.collection(collectionName)
                            .document(postId)
                            .delete()
                            .addOnSuccessListener {
                                Log.d(TAG, "Previous document deleted successfully with ID: $postId")
                            }.addOnFailureListener { e ->
                                Log.e(TAG, "Error deleting previous document with ID: $postId", e)
                            }
                    }.addOnFailureListener { e ->
                        Log.e(TAG, "Error updating post with ID $postId in Firestore", e)
                    }
            }.addOnFailureListener { e ->
                Log.e(TAG, "Error adding post", e)
            }
        }
    }


    /**
     * Obtém os posts do Firestore.
     */
    fun getPosts(){
        viewModelScope.launch {
            db.collection(collectionName).get().addOnSuccessListener {
                val list = mutableListOf<Post>()
                for(document in it.documents){
                    list.add(document.get("post") as Post)
                }
                listPosts.postValue(list)
            }.addOnFailureListener {
                Log.d("Firestore", "query error!")
            }
        }
    }

    /**
     * Obtém os posts em tempo real do Firestore.
     *
     * @param callback Uma função de retorno para processar os posts.
     */
    fun getPostsLive(callback: (List<Post>) -> Unit) {
        viewModelScope.launch {
            val ref = db.collection(collectionName)
            ref.addSnapshotListener { snapshot, e ->
                if (snapshot != null) {
                    val list = mutableListOf<Post>()
                    for (document in snapshot.documents) {
                        val x = document.data
                        val post = x?.get("post") as? Map<*, *>
                        if (post != null) {
                            val id = post["id"] as? String ?: ""
                            val title = post["title"] as? String ?: ""
                            val description = post["description"] as? String ?: ""
                            val author = post["author"] as? String ?: ""
                            val photo = post["photo"] as? String ?: ""
                            val postcode = post["postcode"] as? String ?: ""

                            val latitude = post["latitude"] as? Double ?: 0.0
                            val longitude = post["longitude"] as? Double ?: 0.0

                            val lifespan = (post["lifespan"] as? Long ?: 0).toInt()

                            val typePostStr = post["typePost"] as? String ?: TypePost.CURIOSIDADE.name
                            val typePost = TypePost.valueOf(typePostStr)

                            val upvotes = (post["upvotes"] as? Long ?: 0).toInt()
                            val upvotes_users = post["upvotes_users"] as? List<String> ?: listOf()
                            val downvotes = (post["downvotes"] as? Long ?: 0).toInt()
                            val downvotes_users = post["downvotes_users"] as? List<String> ?: listOf()
                            val createdAt = post["createdAt"] as? Long ?: 0
                            val visible = post["visible"] as? Boolean ?: true
                            val rating = (post["rating"] as? Long ?: 0).toFloat()
                            val rating_users = post["rating_users"] as? List<String> ?: listOf()
                            val comments = post["comments"] as? List<String> ?: listOf()
                            val comments_users = post["comments_users"] as? List<String> ?: listOf()


                            val post = Post(
                                id,
                                title,
                                description,
                                author,
                                photo,
                                postcode,
                                latitude,
                                longitude,
                                lifespan,
                                upvotes,
                                upvotes_users,
                                downvotes,
                                downvotes_users,
                                typePost,
                                createdAt,
                                visible,
                                rating,
                                rating_users,
                                comments,
                                comments_users
                            )
                            list.add(post)
                        }
                    }
                    listPosts.postValue(list)
                    callback(list)
                }
            }
        }
    }

    /**
     * Atualiza o voto de um post para "Upvote".
     *
     * @param postId O ID do post.
     * @param userId O ID do utilizador.
     * @param post O post a ser atualizado.
     */
    fun upvotePost(postId: String, userId: String, post: Post) {
        viewModelScope.launch {
            if (!post.upvotes_users.contains(userId)) {
                val updatedDownvotes = if (post.downvotes_users.contains(userId)) {
                    post.downvotes - 1
                } else {
                    post.downvotes
                }

                val updatedDownvotesUsers = post.downvotes_users.toMutableList().apply { remove(userId) }
                val updatedUpvotes = post.upvotes + 1
                val updatedUpvotesUsers = post.upvotes_users.toMutableList().apply { add(userId) }

                val updatedPost = post.copy(
                    upvotes = updatedUpvotes,
                    upvotes_users = updatedUpvotesUsers,
                    downvotes = updatedDownvotes,
                    downvotes_users = updatedDownvotesUsers
                )

                val postToSave = hashMapOf(
                    "post" to updatedPost
                )

                // Atualiza na Firebase
                db.collection(collectionName)
                    .document(postId)
                    .set(postToSave)
                    .addOnSuccessListener {
                        Log.d(TAG, "Updated post with ID $postId in Firestore")
                    }.addOnFailureListener { e ->
                        Log.e(TAG, "Error updating post with ID $postId in Firestore", e)
                    }

                // Atualiza no Room
                updatedPost(postId, updatedPost)
            } else {
                Log.d(TAG, "User $userId already upvoted this post.")
            }
        }
    }


    /**
     * Atualiza o voto de um post para "Downvote".
     *
     * @param postId O ID do post.
     * @param userId O ID do utilizador.
     * @param post O post a ser atualizado.
     */
    fun downvotePost(postId: String, userId: String, post: Post) {
        viewModelScope.launch {
            if (!post.downvotes_users.contains(userId)) {
                val updatedUpvotes = if (post.upvotes_users.contains(userId)) {
                    post.upvotes - 1
                } else {
                    post.upvotes
                }

                val updatedUpvotesUsers = post.upvotes_users.toMutableList().apply { remove(userId) }
                val updatedDownvotes = post.downvotes + 1
                val updatedDownvotesUsers = post.downvotes_users.toMutableList().apply { add(userId) }

                val updatedPost = post.copy(
                    upvotes = updatedUpvotes,
                    upvotes_users = updatedUpvotesUsers,
                    downvotes = updatedDownvotes,
                    downvotes_users = updatedDownvotesUsers
                )

                val postToSave = hashMapOf(
                    "post" to updatedPost
                )

                // Atualiza na Firebase
                db.collection(collectionName)
                    .document(postId)
                    .set(postToSave)
                    .addOnSuccessListener {
                        Log.d(TAG, "Updated post with ID $postId in Firestore")
                    }.addOnFailureListener { e ->
                        Log.e(TAG, "Error updating post with ID $postId in Firestore", e)
                    }

                // Atualiza no Room
                updatedPost(postId, updatedPost)
            } else {
                Log.d(TAG, "User $userId already downvoted this post.")
            }
        }
    }

    /**
     * Atualiza um post na base de dados local.
     *
     * @param postId O ID do post a ser atualizado.
     * @param updatedPost O post atualizado.
     */
    private fun updatedPost(postId: String, updatedPost: Post) {
        viewModelScope.launch {
            repository.update(updatedPost)
        }
    }


    /**
     * Faz upload de uma imagem para o armazenamento do Firebase (Firebase Storage).
     *
     * @param imageUri A URI da imagem a ser guardada.
     * @param onPostPhotoChange Função de retorno para manipular a URL da imagem.
     */
    fun uploadImageToStorage(imageUri: Uri?, onPostPhotoChange: (String) -> Unit) {
        val imageName = "image_${System.currentTimeMillis()}.jpg"
        imageUri?.let { uriimage -> val storageRef = Firebase.storage.reference.child("images/$imageName")

            storageRef.putFile(uriimage).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    storageRef.downloadUrl.addOnSuccessListener { downloadUri ->
                        val imagemUrl = downloadUri.toString()
                        onPostPhotoChange(imagemUrl)
                    }.addOnFailureListener { exception ->
                        Log.e("tag", "Não foi possível obter o URL")
                    }
                }
            }
        }
    }


    /**
     * Sincroniza os dados entre a base de dados local (Room) e a Firebase, ou seja, obtém todos
     * os posts da Room e guarda na Firebase os que lá faltam
     *
     */
    private suspend fun synchronizeData() {
        // Obter todos os posts da Firebase
        val firebasePosts = listPosts.value ?: listOf()

        // Obter todos os posts da Room
        val roomPosts = allPosts.value ?: listOf()

        // Guardar os posts que faltam na Firebase
        saveMissingPostsToFirebase(roomPosts, firebasePosts)
    }

    /**
     * Guarda os posts ausentes na Firebase.
     *
     * @param roomPosts Lista de posts da base de dados local (Room).
     * @param firebasePosts Lista de posts da Firebase.
     */
    private suspend fun saveMissingPostsToFirebase(roomPosts: List<Post>, firebasePosts: List<Post>) {
        for (roomPost in roomPosts) {
            // Verificar se o post da Room não existe na lista de posts da Firebase
            if (firebasePosts.none { it.id == roomPost.id }) {
                try {
                    // Guardar o post na Firebase
                    val postToSave = hashMapOf(
                        "post" to roomPost
                    )
                    val documentReference = db.collection(collectionName).add(postToSave).await()
                    val postId = documentReference.id

                    // Atualizar o ID do documento para o ID do post
                    val updatedPost = roomPost.copy(id = postId)
                    val updatedPostToSave = hashMapOf(
                        "post" to updatedPost
                    )
                    db.collection(collectionName).document(postId).set(updatedPostToSave).await()

                    // Excluir o documento anterior
                    db.collection(collectionName).document(roomPost.id).delete().await()

                    Log.d(TAG, "Post saved successfully with ID: $postId")
                } catch (e: Exception) {
                    Log.e(TAG, "Error saving post in Firestore", e)
                }
            }
        }
    }

    /**
     * Escuta por alterações na coleção de posts na Firebase e por consequência remove localmente
     * (da room) os posts que foram removidos da Firebase.
     *
     */
    fun listenForPostDeletions() {
        db.collection(collectionName).addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null) {
                for (document in snapshot.documentChanges) {
                    if (document.type == DocumentChange.Type.REMOVED) {
                        // Post removido da Firebase, exclusão da cache local
                        val deletedPostId = document.document.id
                        viewModelScope.launch {
                            deletePostLocally(deletedPostId)
                        }
                    }
                }
            } else {
                Log.d(TAG, "Current data: null")
            }
        }
    }

    /**
     * Elimina um post localmente.
     *
     * @param postId O ID do post a ser excluído.
     */
    suspend fun deletePostLocally(postId: String) {
        repository.deleteById(postId)
    }

    /**
     * Verifica e oculta os posts expirados.
     *
     * Atualiza o campo 'visible' para false nos posts cujo tempo de expiração tenha sido ultrapassado.
     */
    private suspend fun checkAndHideExpiredPosts() {
        val currentTimeInMillis = Calendar.getInstance().timeInMillis

        val allPostsList = allPosts.value ?: return

        for (post in allPostsList) {
            // Tempo para o post expirar em milissegundos
            val postExpirationTime = post.createdAt + (post.lifespan * 24 * 60 * 60 * 1000)

            // Verifica se o tempo de expiração do post é menor que o tempo atual
            if (postExpirationTime < currentTimeInMillis) {
                val updatedPost = post.copy(visible = false)

                updatePostVisibility(updatedPost)
            }
        }
    }

    /**
     * Atualiza a visibilidade de um post na Firebase e na Room.
     *
     * @param post O post a ter sua visibilidade atualizada.
     */
    private suspend fun updatePostVisibility(post: Post) {
        // Atualiza o post na Room
        repository.update(post)

        // Atualiza o post na Firebase
        val postToSave = hashMapOf(
            "post" to post
        )
        db.collection(collectionName).document(post.id).set(postToSave)
            .addOnSuccessListener {
                Log.d(TAG, "Post visibility updated successfully in Firestore")
            }.addOnFailureListener { e ->
                Log.e(TAG, "Error updating post visibility in Firestore", e)
            }
    }

    /**
     * Regista o rating de um post atribuído por um utilizador.
     *
     * @param postId O ID do post avaliado.
     * @param userId O ID do utilizador que fez a avaliação.
     * @param rating A avaliação dada ao post.
     * @param post O post avaliado.
     */
    fun ratePost(postId: String, userId: String, rating: Float, post: Post) {
        viewModelScope.launch {
            if (!post.rating_users.contains(userId)) {
                val updatedRatingUsers = post.rating_users.toMutableList().apply { add(userId) }
                val updatedRating = post.rating + rating

                val updatedPost = post.copy(
                    rating = updatedRating,
                    rating_users = updatedRatingUsers
                )

                val postToSave = hashMapOf(
                    "post" to updatedPost
                )

                // Atualiza na Firebase
                db.collection(collectionName)
                    .document(postId)
                    .set(postToSave)
                    .addOnSuccessListener {
                        Log.d(TAG, "Updated post with ID $postId in Firestore")
                    }.addOnFailureListener { e ->
                        Log.e(TAG, "Error updating post with ID $postId in Firestore", e)
                    }

                // Atualiza no Room
                updatedPost(postId, updatedPost)
            } else {
                Log.d(TAG, "User $userId already rated this post.")
            }
        }
    }

    /**
     * Adiciona um comentário a um post.
     *
     * @param comment O comentário a ser adicionado.
     * @param userId O ID do utilizador que fez o comentário.
     * @param postId O ID do post ao qual o comentário será adicionado.
     * @param user O utilizador que está adicionando o comentário.
     * @param post O post ao qual o comentário será adicionado.
     */
    fun addCommentToPost(comment: String, userId: String, postId: String, user: FirebaseUser?, post: Post) {
        viewModelScope.launch {
            val commentString = "$comment/%/%/${user?.email}"

            val updatedComments = post.comments.toMutableList().apply { add(commentString) }

            val updatedCommentsUsers = post.comments_users.toMutableList().apply { add(userId) }

            val updatedPost = post.copy(
                comments = updatedComments,
                comments_users = updatedCommentsUsers
            )

            val postToSave = hashMapOf(
                "post" to updatedPost
            )

            db.collection(collectionName)
                .document(postId)
                .set(postToSave)
                .addOnSuccessListener {
                    Log.d(TAG, "Updated post with ID $postId in Firestore")
                }.addOnFailureListener { e ->
                    Log.e(TAG, "Error updating post with ID $postId in Firestore", e)
                }

            updatedPost(postId, updatedPost)
        }
    }


}
