package pt.ipp.estg.cidadeinforma.navigation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import kotlinx.coroutines.launch
import pt.ipp.estg.cidadeinforma.database.Post
import pt.ipp.estg.cidadeinforma.database.TypePost
import pt.ipp.estg.cidadeinforma.screens.CreatePost
import pt.ipp.estg.cidadeinforma.screens.FirestoreScreen
import pt.ipp.estg.cidadeinforma.screens.HomeScreen
import pt.ipp.estg.cidadeinforma.screens.PostDetail
import pt.ipp.estg.cidadeinforma.screens.RegisterScreen
import pt.ipp.estg.cidadeinforma.screens.SignInScreen
import pt.ipp.estg.cidadeinforma.viewModels.PostsViewModel

/**
 * Composable para o drawer de navegação.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyNavigatonDrawer(){
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val navController = rememberNavController()
    val drawerItemList = prepareNavigationDrawerItems()
    var selectedItem by remember { mutableStateOf(drawerItemList[0]) }

    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                Spacer(Modifier.height(12.dp))
                drawerItemList.forEach { item ->
                    MyDrawerItem(item,selectedItem,{selectedItem=it},navController,drawerState)
                }
            }
        },
        content = { MyScaffold(drawerState = drawerState, navController = navController)}
    )
}

/**
 * Item do drawer de navegação.
 *
 * @param item O item do drawer.
 * @param selectedItem O item atualmente selecionado.
 * @param updateSelected Callback para atualizar o item selecionado.
 * @param navController O controlador de navegação.
 * @param drawerState O estado do drawer.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyDrawerItem(item:NavigationDrawerData,
                 selectedItem:NavigationDrawerData,
                 updateSelected:(i:NavigationDrawerData)->Unit,
                 navController: NavHostController,
                 drawerState: DrawerState
){
    val coroutineScope = rememberCoroutineScope()
    NavigationDrawerItem(
        icon = { Icon(imageVector = item.icon, contentDescription = null) },
        label = { Text(text = item.label) },
        selected = (item == selectedItem),
        onClick = {
            coroutineScope.launch {
                navController.navigate(item.label)
                drawerState.close()
            }
            updateSelected(item)
        },
        modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
    )
}

/**
 * Composable para o scaffold.
 *
 * @param drawerState O estado do drawer.
 * @param navController O controlador de navegação.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyScaffold(drawerState: DrawerState, navController: NavHostController) {
    val coroutineScope = rememberCoroutineScope()
    Scaffold(
        topBar = {
            MyTopAppBar {
                coroutineScope.launch {
                    drawerState.open()
                }
            }
        },
        content = {padding ->
            Column( modifier = Modifier.padding(padding)){
                MyScaffoldContent(navController)
            }
        }
    )
}

/**
 * Composable para a barra superior.
 *
 * @param onNavIconClick Callback para o clique no ícone de navegação.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyTopAppBar(onNavIconClick: () -> Unit) {
    TopAppBar(
        title = { Text(text = "Cidade Informa") },
        navigationIcon = {
            IconButton(
                onClick = {
                    onNavIconClick()
                }
            ) {
                Icon(
                    imageVector = Icons.Default.Menu,
                    contentDescription = "Open Navigation Items"
                )
            }
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer.copy(alpha = 0.4f)
        )
    )
}


/**
 * Conteúdo do scaffold.
 *
 * @param navController O controlador de navegação.
 */
@Composable
fun MyScaffoldContent(navController: NavHostController){
    NavHost(navController = navController, startDestination = "Home") {
        composable("Home") { HomeScreen(navController) }
        composable("Profile") { SignInScreen(navController) }
        composable("Register") { RegisterScreen(navController)}
        composable("Createpost") { FirestoreScreen(navController)}
        composable("PostDetail/{id}", arguments = listOf(navArgument("id") { type = NavType.StringType })) { backStackEntry ->
            val postId = backStackEntry.arguments?.getString("id")
            if (postId != null) {
                PostDetail(navController, postId)
            }
        }
    }
}

/**
 * Prepara os itens do drawer de navegação.
 *
 * @return A lista de itens do drawer de navegação.
 */
private fun prepareNavigationDrawerItems(): List<NavigationDrawerData> {
    val drawerItemsList = arrayListOf<NavigationDrawerData>()
    drawerItemsList.add(NavigationDrawerData(label = "Home", icon = Icons.Filled.Home))
    drawerItemsList.add(NavigationDrawerData(label = "Profile", icon = Icons.Filled.Person))
    return drawerItemsList
}

/**
 * Classe para representar os dados do item do drawer de navegação.
 *
 * @param label O rótulo do item.
 * @param icon O ícone do item.
 */
data class NavigationDrawerData(val label: String, val icon: ImageVector)
